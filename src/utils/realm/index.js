const DataString = {
    name: "DataString",
    primaryKey: "key",
    properties: {
        key: 'string',
        value: 'string'
    }
}
module.exports = {
    schemaVersion: 1,
    DataString,
    Schemas: [DataString]
}