const ROOT = 'http://123.24.206.9:9215'
// const ROOT = 'http://10.0.50.121:9215'
export default {
    contract: {
        create: ROOT + '/contract/create',
        search: ROOT + '/contract/search',
        delete: ROOT + '/contract/delete',
        update: ROOT + '/contract/update'
    },
    employees: {
        create: ROOT + '/employees/create',
        search: ROOT + '/employees/search',
        delete: ROOT + '/employees/delete',
        update: ROOT + '/employees/update',
        login: ROOT + '/employees/login'
    },
    department: {
        create: ROOT + '/department/create',
        search: ROOT + '/department/search',
        delete: ROOT + '/department/delete',
        update: ROOT + '/department/update'
    },
    insurrance: {
        create: ROOT + '/insurrance/create',
        search: ROOT + '/insurrance/search',
        delete: ROOT + '/insurrance/delete',
        update: ROOT + '/insurrance/update'
    },
    dayOff: {
        create: ROOT + '/day-off/create',
        search: ROOT + '/day-off/search',
        delete: ROOT + '/day-off/delete',
        update: ROOT + '/day-off/update'
    },
    timeKeeping:{
        create: ROOT + '/time-keeping/create',
        search: ROOT + '/time-keeping/search',
        delete: ROOT + '/time-keeping/delete',
        update: ROOT + '/time-keeping/update',
        listFood: ROOT + '/time-keeping/get-employees-by-department',
        checkFood: ROOT + '/time-keeping/refection-day'
    },
    overtime: {
        create: ROOT + '/over-time/create',
        search: ROOT + '/over-time/search',
        delete: ROOT + '/over-time/delete',
        update: ROOT + '/over-time/update'
    }
}