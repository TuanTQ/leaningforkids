import constants from '../utils/constants/';
const defaultState = {
    userApp: {
        test: "go he",
        currentUser: {

        },
        isLogin: false,
        token: ""
    },
    TAB_STATE: 0
}

const reducer = (state = defaultState, action) => {
    var newState = JSON.parse(JSON.stringify(state));
    newState.NAVIGATION = state.NAVIGATION;
    switch (action.type) {
        case constants.TEST:
            newState.userApp.test = action.value
            return newState;
        case constants.TAB_STATE:
            newState.TAB_STATE = action.value
            return newState;
        case constants.NAVIGATION:
            newState.NAVIGATION = action.value
            return newState;
        default:
            return newState;
    }
}

export default reducer;