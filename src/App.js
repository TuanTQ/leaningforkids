import React, { Component } from "react";
import { Root, Text } from "native-base";
import { StackNavigator, TabNavigator } from "react-navigation";
import { Provider } from 'react-redux'
import thunk from 'redux-thunk';
import AppReducer from './reducers/';
import { createStore, applyMiddleware } from 'redux'

import Splash from "./screens/splash/";;
import Home from "./screens/home/"
const AppNavigator = StackNavigator({
  Splash: { screen: Splash },
  Home: { screen: Home }
}, {
  initialRouteName: "Splash",
  headerMode: "none"
});

const store = createStore(AppReducer, applyMiddleware(thunk));
export default () =>
  <Provider store={store}>
    <Root>
      <AppNavigator />
    </Root>
  </Provider>
  ;