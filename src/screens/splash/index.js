import React, { Component } from "react";
import { ImageBackground, View, StatusBar } from "react-native";
import { Container, Button, H3, Text } from "native-base";

import styles from "./styles";

const launchscreenBg = require("../../assets/bg.jpg");
const launchscreenLogo = require("../../assets/logo.jpg");

class Splash extends Component {
  render() {
    return (
      <Container>
        <StatusBar barStyle="light-content" />
        {/* <ImageBackground source={launchscreenBg} style={styles.imageContainer}> */}
          
          <View style={{ marginBottom: 80 }}>
            <Button
              style={{ backgroundColor: "#6FAF98", alignSelf: "center" }}
              onPress={() => this.props.navigation.navigate("Home")}
            ><Text>Lets Go aaaaaaaa!</Text></Button>
          </View>
        {/* </ImageBackground> */}
      </Container>
    );
  }
}

export default Splash;
