import React, { Component } from "react";
import { ImageBackground, View, StatusBar, Image } from "react-native";
import { Container, Content, Button, Text, Header, Left, Body, Right, Title } from "native-base";
import styles from "./styles";
import Card from 'react-native-swiping-cards';
class Home extends Component {
  render() {
    return (
      // <ImageBackground source={require('../../assets/bg.jpg')} style={styles.imageContainer}>
      <View>
        <Card
          onSwipeRight={() => true}
          onSwipeLeft={() => true}
          onSwipeCenter={() => true}
          onRelease={() => true}
          onRightAnimationEnd={() => true}
          onLeftAnimationEnd={() => true}
          onCenterAnimationEnd={() => true}
          onReleaseAnimationEnd={() => true}
        >
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Image source={{ uri: "https://source.unsplash.com/random" }} />
          </View>
        </Card>
      </View>
      // </ImageBackground>
    );
  }
}

export default Home;
